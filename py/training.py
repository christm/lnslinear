#!/usr/bin/env python

from time import time
import numpy as np
import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
import torchvision
import torchvision.transforms as transforms
import py.network.net as net

net = net.Net()

transform = transforms.Compose([transforms.ToTensor()])
start = time()

# load dataset and iterate over samples
train_set = torchvision.datasets.MNIST(root='../data', train=True,
                                        download=True, transform=transform)
train_loader = torch.utils.data.DataLoader(train_set, batch_size=net.batch_size,
                                          shuffle=True)
criterion = nn.MSELoss()
optimizer = optim.SGD(net.parameters(), lr=0.1)
for epoch in range(net.epoch):
    running_loss = 0
    for i, data in enumerate(train_loader, 0):
        inputs, labels = data
        inputs = inputs.view(inputs.shape[0], -1)

        indices = torch.tensor([torch.arange(0,net.batch_size).tolist(), labels])
        labels = torch.sparse_coo_tensor(indices, torch.ones(net.batch_size), [net.batch_size, 10]).to_dense()

        optimizer.zero_grad()
        outputs = net(inputs)
        loss = criterion(outputs, labels)
        loss.backward()
        optimizer.step()

        running_loss += loss.item()
        if i % 200 == 199:    # print every 200 mini-batches
            print('[%d, %5d] loss: %.3f' %
                  (epoch + 1, i + 1, running_loss / 200))
            running_loss = 0.0

print('Finished Training in ', time() - start, 's')
torch.save(net.state_dict(), "network/state_dict.sav")
