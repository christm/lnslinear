from py.utils.configloader import config
import torch
import torch.nn as nn
import torch.nn.functional as F
from py.network.LNSLinear import LNSLinear

class Net(nn.Module):
    """docstring for Net"""
    def __init__(self):
        super(Net, self).__init__()

        self.batch_size = config["training"]["batch_size"]
        self.epoch = config["training"]["epoch"]

        self.fc1 = nn.Linear(784, 300, bias=config["architecture"]["bias"])
        self.fc2 = nn.Linear(300, 100, bias=config["architecture"]["bias"])
        self.fc3 = nn.Linear(100, 10, bias=config["architecture"]["bias"])

    def forward(self, x):
        x = self.fc1(x)
        x = F.relu6(x*6)/6
        x = F.relu6(self.fc2(x)*6)/6
        x = self.fc3(x)
        return x

class LNSNet(nn.Module):
    def __init__(self):
        super(LNSNet, self).__init__()

        self.batch_size = 32
        self.epoch = 10

        self.fc1 = LNSLinear(784, 300, config["architecture"]["input_msb"], config["architecture"]["input_lsb"], config["architecture"]["exp_lsb"], bias=config["architecture"]["bias"])
        self.fc2 = LNSLinear(300, 100, config["architecture"]["input_msb"], config["architecture"]["input_lsb"], config["architecture"]["exp_lsb"], bias=config["architecture"]["bias"])
        self.fc3 = LNSLinear(100, 10,  config["architecture"]["input_msb"], config["architecture"]["input_lsb"], config["architecture"]["exp_lsb"], bias=config["architecture"]["bias"], is_last_layer=True)

    def forward(self, x):
        x = self.fc1(x)
        x = self.fc2(x)
        x = self.fc3(x)
        return x

    def convert(self):
        self.fc1.convert()
        self.fc2.convert()
        self.fc3.convert()
