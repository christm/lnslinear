from py.utils import config, format_bits
import torch
import LNSLinear_cpp
import math
from torch import nn
from torch.autograd import Function
from torch import nn
import getopt
import sys

class LNSLinearFunction(Function):
    @staticmethod
    def forward(ctx, lxs, lws, b, msb, lsb, exp_lsb, is_last_layer):
        return LNSLinear_cpp.forward(lxs, lws, b, msb, lsb, exp_lsb, is_last_layer)

# Layer of custom neurons
class LNSLinear(nn.Module):
    def __init__(self, in_features, out_features, msb, lsb, exp_lsb, bias=True, is_last_layer=False):
        super(LNSLinear, self).__init__()
        self.in_features = in_features
        self.out_features = out_features
        self.msb = msb
        self.lsb = lsb
        self.exp_lsb = exp_lsb
        self.inputLogSize = msb - lsb + 1
        self.weight = nn.Parameter(torch.Tensor(out_features, in_features))
        if(bias == True):
            self.bias = nn.Parameter(torch.Tensor(out_features))
        else:
            self.bias = torch.zeros(out_features)
        self.is_last_layer = is_last_layer

    def forward(self, inputs):
        return LNSLinearFunction.apply(inputs, self.weight, self.bias, self.msb, self.lsb, self.exp_lsb, self.is_last_layer)

    def convert(self):
        self.weight.detach().apply_(self.wlin2log)
        if(not torch.equal(self.bias, torch.zeros(self.out_features))):
            self.bias = nn.Parameter(self.bias * (2**-self.lsb))


    def xlin2log(self, x):
        mask = (1 << self.inputLogSize) - 1
        if x == 0:
            return mask
        shifted_log = int(round(-math.log2(x) * (2**-self.lsb)))
        return min(shifted_log, mask)

    def xlog2lin(self, lx):
        lx = int(lx)
        shifted_lx = -float(lx) * (2**self.lsb)
        return math.pow(2, shifted_lx)

    def wlin2log(self, w):
        mask = (1 << self.inputLogSize) - 1
        if w == 0:
            return mask
        neg = (w < 0) << self.inputLogSize
        shifted_log = int(round(-math.log2(abs(w)) * (2**-self.lsb)))
        logw = min(shifted_log, mask)
        return neg | logw

    def wlog2lin(self, lw):
        lw = int(lw)
        mask = (1 << self.inputLogSize) - 1
        negw = lw >> self.inputLogSize
        logw = -(lw & mask)
        logw = float(logw) * 2**self.lsb
        w = (1 - 2*negw) * (math.pow(2, logw))
        return w
