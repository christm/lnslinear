#!/usr/bin/env python

from py.utils.configloader import config
import numpy as np
import torch
import torch.nn as nn
import torch.nn.functional as F
import torchvision
import torchvision.transforms as transforms
import py.network.net as net
import py.network.LNSLinear as LNSLinear
import sys
import matplotlib
matplotlib.use("TkAgg")
import matplotlib.pyplot as plt

transform = transforms.Compose([transforms.ToTensor()])

if(len(sys.argv) != 2):
    print('Invalid arg number, only 1 is expected')
    exit(-1)

nettype = sys.argv[1]
if(nettype=='base'):
    network = net.Net()
    network.load_state_dict(torch.load("network/state_dict.sav"))
elif (nettype=='lns'):
    network = net.LNSNet()
    network.load_state_dict(torch.load("network/state_dict.sav"))
    network.convert()
else:
    print('Net type unknown')
    exit(-1)

test_set = torchvision.datasets.MNIST(root='../data', train=False,
                                      download=True, transform=transform)
test_loader = torch.utils.data.DataLoader(test_set, batch_size=network.batch_size,
                                          shuffle=False)
correct, total, pseudo_correct = 0, 0, 0
fig, axes = plt.subplots(4, 8, figsize=(1.5*8,2*4))
plot_idx = 0
plot = False

for images, labels in test_loader:
    for i in range(len(labels)):
        img = images[i].view(1, 784)
        with torch.no_grad():
            if(nettype=='base'):
                log_proba = network(img)[0]
            else:
                log_proba = network(img.clone().apply_(network.fc3.xlin2log))[0]

        predicted_labels = (log_proba == torch.max(log_proba)).nonzero(as_tuple=True)[0]
        actual_label = labels[i].item()
        # print(f"{log_proba=}")
        # print(f"{predicted_labels=} instead of {actual_label=}")
        if(predicted_labels.numel() == 1 and predicted_labels[0].item() == actual_label):
            correct += 1
        elif((predicted_labels == actual_label).nonzero(as_tuple=True)[0].numel() > 0):
            print(f"{log_proba=}")
            print(f"{actual_label} is actually in {predicted_labels}, so that counts")
            pseudo_correct += 1

            if(plot == True and plot_idx < 32):
                ax = axes[plot_idx//8, plot_idx%8]
                ax.imshow(images[i][0], cmap='gray_r')
                ax.set_title("label: {}, predicted: {}".format(actual_label, predicted_labels.numpy()))
                plot_idx += 1
                if plot_idx == 32:
                    plt.tight_layout()
                    plt.show()

        total += 1
        if (total%100 == 0):
            print(f'{total} images tested so far, {correct} are valid: {(float(correct)/total * 100):.2f}% precision (but {pseudo_correct} are almost ok)')

print("Number Of Images Tested =", total)
print("\nModel Accuracy =", (correct/total))
if(plot == True and plot_idx < 32):
    plt.tight_layout()
    plt.show()
