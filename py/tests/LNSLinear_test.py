from py.utils.formatter import format_bits
import torch
import unittest
import py.network.LNSLinear as LNSLinear
import sys
import getopt

class LNSLinearUnitTest(unittest.TestCase):
    def test_forward(self):
        print('Testing cpp forward function:')
        print('test msb: 2, test lsb: -3, test exp_lsb: -5')
        x = torch.ones(1, 10, dtype=torch.int) * 0b000_000  # 0 lns, 1 linear
        w = torch.ones(5, 10, dtype=torch.int) * 0b0_111_111 # "0" linear (smallest representable value)
        b = torch.zeros(5, dtype=torch.int)
        w[0][0] = 0b0_001_000 # -1 LNS, .5 linear

        w[1][0] = 0b0_001_000
        w[1][1] = 0b0_001_000

        w[2][0] = 0b0_100_000 # -4 LNS, .0625 linear
        w[2][1] = 0b0_100_000

        w[3][0] = 0b1_100_000 # - -4 LNS, -.0625 linear

        w[4][0] = 0b1_100_000
        w[4][1] = 0b0_100_000
        w[4][2] = 0b0_100_000
        res = LNSLinear.LNSLinearFunction.apply(x, w, b, 2, -3, -5, False)

        # expect .5 linear -> -1 LNS
        print("Returned value is:", format_bits(int(res[0][0]), 8), 'expected value is:', format_bits(0b001_000, 8))
        self.assertEqual(res[0][0], 0b001_000)

        # expect 1 linear -> 0 LNS
        print("Returned value is:", format_bits(int(res[0][1]), 8), 'expected value is:', format_bits(0b000_000, 8))
        self.assertEqual(res[0][1], 0b000_000)

        # expect .125 linear -> -3 LNS
        print("Returned value is:", format_bits(int(res[0][2]), 8), 'expected value is:', format_bits(0b011_000, 8))
        self.assertEqual(res[0][2], 0b011_000)

        # expect 0 linear -> lowest possible value LNS
        print("Returned value is:", format_bits(int(res[0][3]), 8), 'expected value is:', format_bits(0b111_111, 8))
        self.assertEqual(res[0][3], 0b111_111)

        # expect .0625 linear -> -4 LNS
        print("Returned value is:", format_bits(int(res[0][4]), 8), 'expected value is:', format_bits(0b100_000, 8))
        self.assertEqual(res[0][4], 0b100_000)

    def test_xlog2lin(self):
        print()
        print('Testing activation conversion to linear')
        print('test msb: 4, test lsb: -4, test exp_lsb: -4')
        layer = LNSLinear.LNSLinear(784, 300, 4, -4, -4, bias=False)
        res = layer.xlog2lin(0b00000_0000)
        print(f'xlog2lin({format_bits(0b00000_0000, 9)}) = {res}')
        self.assertEqual(res, 1)
        res = layer.xlog2lin(0b00001_0000)
        print(f'xlog2lin({format_bits(0b00001_0000, 9)}) = {res}')
        self.assertEqual(res, 0.5)
        res = layer.xlog2lin(0b00010_0000)
        print(f'xlog2lin({format_bits(0b00010_0000, 9)}) = {res}')
        self.assertEqual(res, 0.25)
        res = layer.xlog2lin(0b11111_1111)
        print(f'xlog2lin({format_bits(0b11111_1111, 9)}) = {res}')
        self.assertEqual(res, 2**-(2**5 - 2**-4))

    def test_xlin2log(self):
        print()
        print('Testing activation conversion to lns')
        print('test msb: 2, test lsb: -4, test exp_lsb: -4')
        layer = LNSLinear.LNSLinear(784, 300, 2, -4, -4, bias=False)
        res = layer.xlin2log(0)
        print(f'input_conversion(0)         = {format_bits(res)}')
        self.assertEqual(res, 0b111_1111)
        res = layer.xlin2log(1)
        print(f'input_conversion(1)         = {format_bits(res)}')
        self.assertEqual(res, 0b000_0000)
        res = layer.xlin2log(0.25)
        print(f'input_conversion(0.25)      = {format_bits(res)}')
        self.assertEqual(res, 0b010_0000)
        res = layer.xlin2log(0.125)
        print(f'input_conversion(0.125)     = {format_bits(res)}')
        self.assertEqual(res, 0b011_0000)
        res = layer.xlin2log(2**-6)
        print(f'input_conversion(2**-6)     = {format_bits(res)}')
        self.assertEqual(res, 0b110_0000)
        res = layer.xlin2log(2**-7.95)
        print(f'input_conversion(2**-7.95)  = {format_bits(res)}')
        self.assertEqual(res, 0b111_1111)
        res = layer.xlin2log(2**-8)
        print(f'input_conversion(2**-8)     = {format_bits(res)}')
        self.assertEqual(res, 0b111_1111)


    def test_wlog2lin(self):
        print()
        print('Testing weight conversion to linear')
        print('test msb: 3, test lsb: -4, test exp_lsb: -4')
        layer = LNSLinear.LNSLinear(784, 300, 4, -4, -4, bias=False)
        res = layer.wlog2lin(0b0_00000_0000)
        print(f'wlog2lin({format_bits(0b0_00000_0000, 10)}) = {res}')
        self.assertEqual(res, 1)
        res = layer.wlog2lin(0b0_00001_0000)
        print(f'wlog2lin({format_bits(0b0_00001_0000, 10)}) = {res}')
        self.assertEqual(res, 0.5)
        res = layer.wlog2lin(0b0_00010_0000)
        print(f'wlog2lin({format_bits(0b0_00010_0000, 10)}) = {res}')
        self.assertEqual(res, 0.25)
        res = layer.wlog2lin(0b0_11111_1111)
        print(f'wlog2lin({format_bits(0b11111_1111, 10)}) = {res}')
        self.assertEqual(res, 2**-(2**5 - 2**-4))
        res = layer.wlog2lin(0b1_00000_0000)
        print(f'wlog2lin({format_bits(0b1_00000_0000, 10)}) = {res}')
        self.assertEqual(res, -1)
        res = layer.wlog2lin(0b1_00001_0000)
        print(f'wlog2lin({format_bits(0b1_00001_0000, 10)}) = {res}')
        self.assertEqual(res, -0.5)
        res = layer.wlog2lin(0b1_00010_0000)
        print(f'wlog2lin({format_bits(0b1_00010_0000, 10)}) = {res}')
        self.assertEqual(res, -0.25)
        res = layer.wlog2lin(0b1_11111_1111)
        print(f'wlog2lin({format_bits(0b11111_1111, 10)}) = {res}')
        self.assertEqual(res, -2**-(2**5 - 2**-4))


    def test_wlin2log(self):
        print()
        print('Testing weight conversion to lns')
        print('test msb: 3, test lsb: -4, test exp_lsb: -')
        layer = LNSLinear.LNSLinear(784, 300, 3, -4, -4, bias=False)
        res = layer.wlin2log(0)
        print(f'weight_conversion(0)         = {format_bits(res, 10)}')
        self.assertEqual(res, 0b0_1111_1111)
        res = layer.wlin2log(1)
        print(f'weight_conversion(1)         = {format_bits(res, 10)}')
        self.assertEqual(res, 0b0_0000_0000)
        res = layer.wlin2log(0.25)
        print(f'weight_conversion(0.25)      = {format_bits(res, 10)}')
        self.assertEqual(res, 0b0_0010_0000)
        res = layer.wlin2log(2**-8)
        print(f'weight_conversion(2**-8)     = {format_bits(res, 10)}')
        self.assertEqual(res, 0b0_1000_0000)
        res = layer.wlin2log(2**-70)
        print(f'weight_conversion(2**-70)    = {format_bits(res, 10)}')
        self.assertEqual(res, 0b0_1111_1111)
        res = layer.wlin2log(-1)
        print(f'weight_conversion(-1)        = {format_bits(res, 10)}')
        self.assertEqual(res, 0b1_0000_0000)
        res = layer.wlin2log(-0.25)
        print(f'weight_conversion(-0.25)     = {format_bits(res, 10)}')
        self.assertEqual(res, 0b1_0010_0000)

if __name__ == "__main__":
    argv = sys.argv[1:]
    opts, args = getopt.getopt(argv, '')
    unittest.main()
