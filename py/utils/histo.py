import py.network.net as net
from py.network.LNSLinear import LNSLinear
from py.utils import config
import math
import torch
import numpy as np
import matplotlib.pyplot as plt

old_net = net.Net()
old_net.load_state_dict(torch.load("../network/state_dict.sav"), strict=False)
ptiles = [1, 10, 25, 50, 75, 90, 99]
weights = [old_net.fc1.weight.detach().numpy(), old_net.fc2.weight.detach().numpy(), old_net.fc3.weight.detach().numpy()]
bins = 100

fig, ax = plt.subplots(2, weights.__len__())
i = 0

print('Stats for vanilla MLP net')
for w in weights:
    sorted_w = np.sort(np.ndarray.flatten(w))
    print(f'Number of weights: {sorted_w.__len__()}')
    print(f'Min: {np.ndarray.min(sorted_w)}, Max: {np.ndarray.max(sorted_w)}')
    print(f'{ptiles}th percentiles:')
    print(f'{np.percentile(sorted_w, ptiles)}')
    print(f'First 10: {sorted_w[:10]}')
    print(f'Last 10: {sorted_w[-10:]}')
    print()
    _ = ax[0, i].hist(sorted_w, bins=bins)  # arguments are passed to np.histogram
    ax[0, i].set_title(f"Original net fc{i}")
    i = i + 1

new_net = net.LNSNet()
new_net.load_state_dict(torch.load("../network/state_dict.sav"), strict=False)
new_net.convert()
def w_conv(x):
    return math.log2(abs(x))
    # return new_net.fc1.wlog2lin(x)
w_conv_vect = np.vectorize(w_conv)
weights = [new_net.fc1.weight.detach().numpy(), new_net.fc2.weight.detach().numpy(), new_net.fc3.weight.detach().numpy()]
i = 0

print()
print('Stats for LNS net with converted weights')
for w in weights:
    # sorted_w = np.sort(np.ndarray.flatten(w)).astype(int)
    sorted_w = w_conv_vect(np.sort(np.ndarray.flatten(w)).astype(int))
    print(f'Number of weights: {sorted_w.__len__()}')
    print(f'Min: {np.ndarray.min(sorted_w)}, Max: {np.ndarray.max(sorted_w)}')
    print(f'{ptiles}th percentiles:')
    print(f'{np.percentile(sorted_w, ptiles)}')
    print(f'First 10: {sorted_w[:10]}')
    print(f'Last 10: {sorted_w[-10:]}')
    print()
    _ = ax[1, i].hist(sorted_w, bins=bins)  # arguments are passed to np.histogram
    ax[1, i].set_title(f"New net fc{i}")
    i = i + 1

plt.show()
