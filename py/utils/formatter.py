import math

def format_bits(word, max_length=8, block_size=4):
    format_str = '{:0' + str(max_length) + 'b}' # max length to fill with 0
    str_rep = format_str.format(word)
    res = ''
    str_len = str_rep.__len__()
    for i in range(math.ceil(str_rep.__len__()/block_size), 0, -1):
        start = str_len - block_size*i
        end = start + block_size
        start = 0 if start < 0 else start
        res += str_rep[start:end] + ' '
    return res[0:-1] # cut last space
