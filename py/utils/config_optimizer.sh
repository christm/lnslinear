#!/usr/bin/bash

cd ../
best_prec=0
best_config=''
i=0

for (( msb = 2; msb < 5; msb++ )); do
    for (( lsb = 0; lsb > -2; lsb-- )); do
        for (( exp_lsb = -3; exp_lsb > -7; exp_lsb-- )); do
            config="msb: ${msb}, lsb: ${lsb}, exp_lsb: ${exp_lsb}"
            sed -Ei "s/(\\\"input_msb\\\": )-?[0-9]*/\1${msb}/g" utils/config.json
            sed -Ei "s/(\\\"input_lsb\\\": )-?[0-9]*/\1${lsb}/g" utils/config.json
            sed -Ei "s/(\\\"exp_lsb\\\": )-?[0-9]*/\1${exp_lsb}/g" utils/config.json
            prec=`python testing.py lns | grep "Model Accuracy" | grep -oE "0\.[0-9]*"`
            echo "${config} yields ${prec}"
            if (( $(echo "$prec > $best_prec" | bc -l) )); then
                echo "New best!"
                best_prec=$prec
                best_config=$config
            fi
        done
    done
done

echo "Best prec is ${best_prec} for ${best_config}"
cd utils
