import json, os, inspect

dirname = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
with open(dirname + "/config.json", "r") as f:
    text_config = f.read()
config = json.loads(text_config)
del text_config, dirname
