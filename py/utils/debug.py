#!/usr/bin/env python

import py.network.net as net
import py.network.LNSLinear as LNSLinear
import torch
import torch.nn as nn
import torchvision
import torchvision.transforms as transforms
import math

def format_bits(word, max_length=8, block_size=4):
    format_str = '{:0' + str(max_length) + 'b}' # max length to fill with 0
    str_rep = format_str.format(word)
    res = ''
    str_len = str_rep.__len__()
    for i in range(math.ceil(str_rep.__len__()/block_size), 0, -1):
        start = str_len - block_size*i
        end = start + block_size
        start = 0 if start < 0 else start
        res += str_rep[start:end] + ' '
    return res[0:-1] # cut last space

transform = transforms.Compose([transforms.ToTensor()])

original_net = net.Net()
original_net.load_state_dict(torch.load("../network/state_dict.sav"), strict=False)
new_net = net.LNSNet()
new_net.load_state_dict(torch.load("../network/state_dict.sav"), strict=False)
new_net.convert()

test_set = torchvision.datasets.MNIST(root='../../data', train=False,
                                      download=True, transform=transform)
test_loader = torch.utils.data.DataLoader(test_set, batch_size=1,
                                          shuffle=True)
i = 0
for images, label in test_loader:
    img = images[0].clone().view(1,784)
    expected = original_net.fc1.forward(img)
    actual = new_net.fc1.forward(img.clone().apply_(new_net.fc1.xlin2log))

    summands = original_net.fc1.weight[1] * img
    for i in range(784) :
        if summands[0][i] != 0:
            print(i, "th element: ", img[0][i].item(), " * ", original_net.fc1.weight[1][i].item(), " = ", summands[0][i].item())
            print("input: ", format_bits(new_net.fc1.xlin2log(5, -4)(img[0][i].item()), 10), ", weight: ", format_bits(new_net.fc1.wlin2log(original_net.fc1.weight[1][i].item()), 10))
    expected = (nn.ReLU6()(expected*6)/6)[0].clone().detach().apply_(new_net.fc1.xlin2log(5, -4))
    print("expected is", expected)
    print("actual is", actual[0])
    print("diff is: ", actual[0] - expected)

    exit(0)
