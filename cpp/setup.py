import os, inspect
from setuptools import setup
from torch.utils.cpp_extension import BuildExtension, CppExtension

path_to_ext_lib = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe()))) + "/../ext_lib"

setup(
    name='LNSLinear_cpp',
    ext_modules=[
        CppExtension('LNSLinear_cpp', ['LNSLinear.cpp'], libraries=['gmpxx', 'mpfr', 'lns', 'gmp'],
                     library_dirs=[path_to_ext_lib],
                     runtime_library_dirs=[path_to_ext_lib],
                     extra_compile_args=['-fopenmp']),
    ],
    cmdclass={
        'build_ext': BuildExtension
    })
