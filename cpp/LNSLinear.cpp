#include <torch/extension.h>
#include <iostream>
#include <bitset>
#include <gmpxx.h>
#include <omp.h>
#include <stdint.h>

namespace flopoco {
    extern int64_t do_emulate(int64_t lxs[], int64_t lws[], int64_t b, int msb, int lsb, int prevLayerWidth,
                                int expLsb, bool isLastLayer);
}

using namespace std;

/* @brief Unpacks torch tensors to compute activation. The method expects to find
 * integers tensors, unpacks them as mpz_class for the computation and packs the
 * result as integer torch tensor.
 *.
 * @param x The input tensor, expecting a {1, n} sized integer tensor.
 * @param w The weights tensor, expecting a {m, n} sized integer tensor.
 * @param b The bias tensor, expecting a {m} sized integer tensor
 * @param msb Index of the msb for the computation word format
 * @param lsb Index of the lsb for the computation word format
 *
 * @returns A {1, m} tensor with the computation results as integers
 */
torch::Tensor LNSLinear_forward(torch::Tensor x, torch::Tensor w, torch::Tensor b, int msb, int lsb,
                                int expLsb, bool isLastLayer) {
    x = x.toType(torch::kInt64);
    w = w.toType(torch::kInt64);
    b = b.toType(torch::kInt64);
    int prevLayerWidth = x[0].size(0);
    int layerWidth = w.size(0);
    int64_t* bptr = (int64_t*)b.data_ptr();
    auto res = torch::ones({1, layerWidth}, torch::kInt64);

    #pragma omp parallel for
    for(int i = 0; i < layerWidth; i++) {
        int64_t lxs[prevLayerWidth], lws[prevLayerWidth];
        // if(i != 1)
        //     continue;
        int64_t* xptr = (int64_t*)x[0].data_ptr();
        int64_t* wptr = (int64_t*)w[i].data_ptr();
        for(int j = 0; j < prevLayerWidth; j++) {
            lxs[j] = *(xptr+j);
            lws[j] = *(wptr+j);
        }

        res[0][i] = flopoco::do_emulate(lxs, lws, *(bptr+i), msb, lsb, prevLayerWidth, expLsb, isLastLayer);
    }
    return res;
}

PYBIND11_MODULE(TORCH_EXTENSION_NAME, m) {
  m.def("forward", &LNSLinear_forward, "LNSLinear forward");
}
