\documentclass[11pt, a4paper]{article}
\input{doc_defs}

\begin{document}
    \begin{titlepage}
        \begin{minipage}[t]{0.5\textwidth}
            \begin{flushleft}
                \includegraphics[scale=0.05]{uga.png}
            \end{flushleft}
        \end{minipage}
        \begin{minipage}[t]{0.5\textwidth}
            \begin{flushright}
                \today
            \end{flushright}
        \end{minipage}
        \vspace*{6cm}

        \begin{center}
            \Huge
            \textbf{LNSLinear neural network}
        \end{center}

        \vspace{10cm}
        \normalsize
        \begin{minipage}[t]{0.5\textwidth}
            \begin{flushleft}
                Supervisors:\\
                Pr. Frédéric Pétrot\\
                Pr. Florent Dupont de Dinechin
            \end{flushleft}
        \end{minipage}
        \begin{minipage}[t]{0.5\textwidth}
            \begin{flushright}
                Author:\\
                Maxime Christ
            \end{flushright}
        \end{minipage}
    \end{titlepage}

    \section{Introduction}
        After reviewing the literature on hardware accelerators design for Deep Neural Networks (DNN) such as \cite{nakahara:objectdetector}, \cite{tao:trainingsummary} and \cite{zhao:fpgabnn}, it appears that the main issues are the memory bandwidth bottleneck and the number of Processing Entity (PE) that we can fit on a board.
        For real life networks, it is not possible to simply affect one PE to each neuron. This means that weights needs to be moved around for a PE to emulate different neurons. The fewer PE, the more data movement is needed.

        We can identify 2 development axes:
        \begin{itemize}
            \item make smaller PEs
            \item reduce data movement ($e.g.$: quantize weights, in memory computing, ...)
        \end{itemize}
        We decided to start by looking into how to reduce the PE area footprint, and since there is inference pass before each learning pass, we focused on the inference context.

        Inference in low precision being already largely covered, we decided to follow \cite{johnson:lns} idea that allows the replacement of multipliers by adders, which are usually smaller to implement.
        This can be achieved by representing weights and activation in Logarithmic Number System (LNS). We use a fixed point format and a reduced bit-width.
        The goal that has been fixed is to implement a DNN on FPGA with custom LNS neurons and evaluate both its hardware and software performances: circuit area, throughput and power consumption and network classification accuracy.

        The next sections present the selected neuron hardware design, the software architecture used to evaluate the results and the results produced.

    \section{Logarithmic neuron}
    \subsection{Technical setup}
        \label{section:tech-setup}
        The objective of this project is to bind 2 frameworks:
        \begin{itemize}
            \item \flopoco, an open-source C++ written VHDL generator to produce the hardware design.
            \item \pytorch, an open-source Python machine-learning tool that helps you play around with a high-level interface.
        \end{itemize}
        We extend \flopoco\ by adding a neuron operator and we evaluate the classification accuracy of our flow with \pytorch.
        The problem is that these frameworks are not available in the same language (partially wrong, \pytorch\ has some C++ helpers but only a portion of the interface is available).
        To avoid writing the neuron's flow in both C++ and Python, and with the help of the \pytorch's cpp extension we wrote a wrapper to execute C++ code in a python environment.
        This way we can write once a C++ emulation function that will be used by both \pytorch\ to compute the neuron output and by \flopoco\ to build the VHDL test benches.

        The setup is very standard and eases the hardware design exploration, it is not limited to the lnslinear neuron flow proposed here. It allows the developer to measure the classification accuracy of a network with specific bit-precise computation without needing to go through the whole hardware implementation workflow.
        \pytorch\ brings easy of use classes and wrappers to load datasets, build custom networks, train efficiently, etc. Its abstraction provides flexibility that hardware design often lacks of.
        I haven't yet found a way to generate either the VHDL from this bit-precise C++ method or the other way around, so there's still "duplicate" code for that: a C++ method to compute the bit-precise result and another one to produce the VHDL code to compute the same result.
        Please see  TODO %TODO: LINK FLOPOCO GIT
        to see how to interface with \flopoco.

        We implement a regular MultiLayer Perceptron (MLP).
        It consists in 3 Fully-Connected (FC) layers of 300, 100 and 10 neurons.
        %TODO: check usual number of neurons
        We use ReLU-1 as the activation function for all layers.
        The network is trained in standard floating point 32 bits precision in pytorch against the MNIST dataset, split into a \numprint{60000} training samples and \numprint{10000} validation samples. The images are a transcription of handwritten digits formatted in 28$\times$28 grey level pixels.
        The learnt parameters and input are then converted to our representation system (covered in section \ref{section:encoding}).
        We run the bit-precise simulation against the validation set to evaluate the classification accuracy.


    \subsection{Terminology and encoding}
        \label{section:terminology}
        The main idea is not to encode weights and activations but rather their logarithm.
        We call the weight value \W and the activation value \X.
        The opposites of their rounded log to the given precision are called respectively \lw\ and \lx.
        \begin{align*}
            \lw &= \round{-log(\W)} \\
            \lx &= \round{-log(\X)}
        \end{align*}

        The ReLU-1 activation function that we use (see section \ref{section:tech-setup}) ensures that the activations \X\ are limited to the $[0, 1]$ interval, so $\lw >= 0$.
        We've also determined empirically that $|w| <= 1$, so $\lw >= 0$ as well.
        However, the weights can be negative so we need an extra sign bit \sw\ to be able to represent \W\ in the $\left[-1, 0 \right[$ interval.

        We call \inw\ and \inx\ the concatenation of the flags and the \lw and \lx.
        Figure \ref{fig:inputs} illustrates the construction of these input signals.

        \begin{figure}
            \begin{tikzpicture}[x=5ex, y=5ex]
                \draw (-9, .5) node[right]{\inw};
                \foreach \i in {-1,...,5} {
                    \draw (-\i,0) rectangle++(1,1);
                }
                \foreach \i in {-1,...,4} {
                    \draw (-\i + .5, .5) node{$lw_{\i}$};
                }
                \draw (-5 + .5, .5) node{\sw};

                \draw (-9, 2.5) node[right]{\inx};
                \foreach \i in {-1,...,4} {
                    \draw (-\i, 2) rectangle++(1,1);
                    \draw (-\i + .5, 2.5) node{$lx_{\i}$};
                }

                \draw[<->](-4, -.5) -- ++ (6, 0) node[midway, below]{inputLogSize};
            \end{tikzpicture}
            \caption{Input representation for $msb=4$ and $lsb=-1$ \label{fig:inputs}}
        \end{figure}

    \subsection{Neuron's flow}
        \input{LNSNeuron}

        Figure \ref{fig:lnsneuron} is a detailed graph showing the different operations performed to compute the neuron's activation.
        As a reminder, what we are trying to compute here is
        \begin{equation*}
            f(\W s, \X s) = act \left(\sum_{i = 0}^{n} W_i \times X_i\right)
        \end{equation*}
        with
        \begin{equation*}
            act(x) = ReLU1(x) = \left\{\begin{array}{lr}
                0   &   \text{for } x < 0 \\
                x   &   \text{for } 0 \leq x \leq 1 \\
                1   &   \text{for } x > 1\\
            \end{array}
            \right.
        \end{equation*}

        As mentioned before, the goal of the LNS is to convert all these multiplications into additions. However, the cost of the regular addition gets bigger. For this reason we convert the product $\W \times \X$ back to linear domain before the accumulation.

        This conversion can be costly but since we use low bit-width representation of $log(w)$ and $log(x)$, we can tabulate it. Moreover, when targeting FPGA, if we manage to keep $\lw + \lx$ bit-width under 6 bits, the implementation cost of such a table is only $n$ LUTs, where $n$ is the function output bitwidth.

        Finally we need to convert the result back to LNS before outputting it for the next layer.
        There is a large variety of available activation functions, and some of them have complex hardware implementation\cite{tsmots:hw-sigmoid}\cite{pasca:fpga-act-functions}.
        In order to be flexible with a constant cost, and since we are in low precision we decided to tabulate the activation function as well.
        In addition, it allows us to pre-compute $f(x) = \round{-log(act(x))}$ instead of $f(x) = act(x)$.
        Our choice in activation functions is therefore restricted (the output range must be in $[0, 1]$), but it completely mitigates cost of the conversion from linear to LNS. In this work, we are going to use ReLU-1 as an example.
        It limits the overhead of translating back and forth to LNS and linear domain to just the area needed by the $2^x$ tables.
        Our hope is that this overhead is compensated by using adders instead of multipliers.

        \subsection{Experimental results and ongoing improvements}
            As of today, with a network trained with no bias during 25 epochs, with a batch-size of 32 and a learning rate of 0.1 we achieve 98.03\% classification accuracy. This is not state of the art precision and it can be improved with more epochs and an adaptive learning rate, but we considered it sufficient to work with. To present the final results of this experimentation in an article, we will of course compare to and work with weights that get state of the art accuracy.

            With our custom set of weights (represented in the histograms figure \ref{fig:histo}) and our neuron simulation we achieve 97.67\% accuracy. After exploring the implementation space in terms of bit-width we concluded that this is the best accuracy we could get without further training or tricks (data augmentation, fine-tuning post quantization, ...). The parameters allowing these results are the following: \lw\ and \lx are coded on 4 bits ($msb=2$ and $lsb=-1$) and the $2^{-x}$ table output has 8 bits ($msb=1$ and $lsb=-6$).

            \begin{figure}
                \includegraphics[width=\textwidth]{histo.png}
                \caption{Histograms of the trained weights of each layer. \label{fig:histo}}
            \end{figure}

            We are still in the process of producing the VHDL design so we have no results on the hardware design part for now.
        \subsection{Future work}
        \subsubsection{LNSLinear in real world models}
            The results are encouraging with our toy model, but an evaluation of the performance against real-world models and datasets is in order. Either in networks composed exclusively of FC layers or in hybrid models with standard convolutions (in this case we won't try and fit the design on a FPGA board).
        \subsubsection{LNSConv}
            Another lead on future work would be to implement LNS, low bit-width convolution layers. However we can already expect a lower gain in area since a convolutional neuron only performs as many multiplications as the kernel size instead of one per neuron in the previous layer.
        \subsubsection{Learning in very low precision}
            \label{sec:future-work-learning}
            The first objective of my thesis was to focus on the learning phase, which I haven't done yet.
            There are then 2 possibilities:
            \begin{itemize}
                \item quantize a standard network, train it with reduced word precision and try to fit it on a board.
                Some previous works such as \cite{prostboucle:tnn} and \cite{courbariaux:lowprec} have inspiring results that could lead to a training accelerator.
                \item obviously, look at the training in the LNS context.
            \end{itemize}

    \newpage
    \printbibliography
\end{document}
