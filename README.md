## Introduction
Welcome to LNSLinear! In this repository you will find how to bring C++ code
into a pytorch neural network. The C++ code performs bitwise computation of
the neuron's output, then the pytorch script evaluates its classification
accuracy when used in a basic Multi-Layer Perceptron against the MNIST dataset.


This particular example focuses on reducing the bitwidth of both weights and
activations and using the Logarithmic Number System (LNS) to ease the hardware
design intended to appear in [Flopoco](http://flopoco.gforge.inria.fr/) in a
near future.


Here are the libraries required for the project to work:

### Python
- A very simple neural net is implemented with [Pytorch](https://pytorch.org/)
to evaluate the performance of the neuron in terms of classification accuracy.

- [Setuptools](https://pythonhosted.org/an_example_pypi_project/setuptools.html)
which helps [CppExtension](https://pytorch.org/docs/stable/cpp_extension.html)
(part of the torch package) build a python package using cpp code.

### C++
- [MPFR](https://www.mpfr.org/) provides a set of C++ types and methods to
represent and work with custom precision numbers, either floating point or fixed
point. It is installed by default on Ubunutu 20.04.

- Since the code to emulate a single neuron is custom and implemented in C++,
the run time can be significantly greater than the GPU optimised built-ins of
pytorch. However, each neuron is independant so we can leverage a valuable
speed up with parallelism. Therefore, [OpenMP](https://www.openmp.org/) has been
selected to effortlessly multithread the main loop. It is included in most
common compiler (`-fopenmp` for `g++`) and need no installation.

## Installation guide

For the bridge to work the compiler building the C++ extension has to be ABI
compatible with the compiler Pytorch was built with (personally the `g++` latest
version worked fine). I largely relied on
[this](https://pytorch.org/tutorials/advanced/cpp_extension.html) tutorial to
come up with this solution.

Here is the python version I used
```
$ python --version
Python 3.8.10
```
There is a `requirements.txt` file to install all the python dependencies.
For pip users, run
```
pip install -r requirements.txt
```

Concerning the C++ libraries, as mentioned earlier, both of them are installed
by default (on Ubuntu systems for MPFR and with g++ for OpenMP). You might need
to install ninja on your own though. __FIXME: check need for ninja__

## Build

The C++ code brings the need for a compilation step (actually 2). The first one
consists in buildìng a dynamic lib out of the git submodule found here __TODO:
put submodule git repo here.__. (_This code is also used by flopoco, the whole
point of this project is to link flopoco and pytorch together for hardware neuron
design_).
```
cd ext_lib
g++ PytorchBridge.cpp -o PytorchBridge.o -c -fPIC
g++ -shared -fPIC -o liblns.so PytorchBridge.o -lmpfr -lgmpxx
cd ..
```

The second and final stage consists in building a python package from the C++
wrapper of the external lib. It is rather simple now that we have setuptools and
CppExtension:
```
cd cpp
python setup.py install
cd -
```

In order to run the project expects to find a `py/utils/config.json` file with
certain keys. A working example is provided here: `py/utils/config.json.sample`.
Please copy it with
```
cp py/utils/config.json.sample py/utils/config.json
```
or write your own file before proceeding to the next section.

## Usage

This project only focuses on inference, the network still needs to be trained in
a regular manner. A python script named `training.py` is there to do the job. It
will generate a `state_dict.sav` file that stores all learnt weights and biases
for later usage. The learning hyperparameters such as the batch size, number of
epochs and learning rate can be adjusted in the `config.json` file. You can then
start the training with:
```
python training.py
```

Once trained, the `testing.py` script evaluates the classification accuracy of
both baseline and LNS networks. Parameters such as wights and biases are
expected to be found in the `state_dict.sav` file
```
python testing.py base
```

To test the LNS network, type:
```
python testing.py lns
```

Please note that it runs the C++ code thousands of time and that it is slow due
to custom precision types of MPFR. Consider using the `OMP_NUM_THREAD`
environment variable to set the number of threads you want to run in parallel to
reduce the execution time. If not set, the default value used by OpenMP is 4.

You can play around with the bitwidth by changing the values found under the
`architecture` section of the `config.json` file.
