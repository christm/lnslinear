#include <iostream>
#include <bitset>
#include <math.h>
#include "PytorchBridge.hpp"

#include<iostream>
#include<fstream>

using namespace std;
namespace flopoco {
    void debug_print(int index, int64_t tmplx, int64_t tmplw, int64_t lx, int64_t lw, int64_t lwx, double lwxFloat,
                     double wxFloat, int64_t summand, int64_t acc) {
            cout << index << "th element:" << endl;
            cout << "received lw: " << bitset<10>(tmplw) << endl;
            cout << "received lx: " << bitset<10>(tmplx) << endl;
            cout << "lw: " << bitset<10>(lw) << " or " << lw << endl;
            cout << "lx: " << bitset<10>(lx) << endl;
            cout << "lwx: " << bitset<10>(lwx) << " or " << double(lwx) << endl;
            cout << "lwxFloat: " << lwxFloat << endl;
            cout << "wxFloat: " << wxFloat << endl;
            cout << "summand: " << bitset<10>(summand);
            cout << ", acc: " << bitset<10>(acc) << endl;
    }

    // lx =   xxxx xxxx   -log(x) in [0, 2^(msb+1) - 2^lsb] => x in ]0, 1]
    // lw = S xxxx xxxx   -log(w) in [0, 2^(msb+1) - 2^lsb] => w in [-1, 1[ without 0
    int64_t do_emulate(int64_t lxs[], int64_t lws[], int64_t bias, int msb, int lsb, int prevLayerWidth,
                         int expLsb, bool isLastLayer) {
        int64_t tmplx, tmplw, valFilter, filtered_lw, lx, lw, signlw, og_lwx, lwx, summand, acc, a;
        double wxFloat, accFloat, loga;
        int inputLogSize = msb - lsb + 1;
        valFilter = (1ll << inputLogSize) - 1;
        acc = bias; // Virtually useless, we could accumulate in b

        for (int i = 0; i < prevLayerWidth; i++) {
            tmplx = lxs[i];
            tmplw = lws[i];

            bool negw = tmplw >> inputLogSize;
            lw = tmplw & valFilter; // unsigned, -log(w)
            lx = tmplx; // unsigned, -log(x)
            lwx = lw + lx; // -log(wx)

            double lwxFloat = -double(lwx) * exp2(lsb); // exact, log(wx), alignment
            wxFloat = exp2(lwxFloat); // rounding @lsb=-53, 2**x
            wxFloat *= exp2(-expLsb); // exact, alignment
            summand = int64_t(round(wxFloat)); // rounding @expLsb TODO: round or truncate ?
            summand = summand & ((1ll << (2 - expLsb)) - 1); // Making sure summand is small

            if(negw)
                acc -= summand;
            else
                acc += summand;
            // debug_print(i, tmplx, tmplw, lx, lw, lwx, lwxFloat, wxFloat, summand, acc);
        }

        // ofstream file;
        // file.open("activations.csv", ios::app);
        // file << double(acc) / (1ll << -expLsb) << ", ";
        // file.close();
        // if(abs(double(acc) * exp2(expLsb)) > 5)
        //     cout << "Bang " << double(acc) * exp2(expLsb) << endl;

        // Clipped RELU activation function
        if(isLastLayer == false) {
            if (acc > (1ll << -expLsb)) // High cut
                acc = 1ll << -expLsb;

            if(acc <= 0) // Low cut
                a = valFilter; // Smallest representable value
            else { // Translate back to log domain
                accFloat = double(acc) * exp2(expLsb); // exact
                loga = log2(accFloat); // rounding @lsb=-53
                loga *= exp2(-lsb);
                a = int64_t(round(-loga)); // >0, unsigned, rounding @lsb

                if(a > valFilter) {
                    // cout << "Overflow ! Diff is: " << -a - valFilter << endl;
                    a = valFilter;
                }
            }
        } else {
            if(acc <= 0) // Low cut
                a = 0; // Smallest respresentable value
            else { // Final layer doesn't need to restrict bit width nor tranlation to log domain
                a = acc;
            }
        }

        return a;
    }
}
