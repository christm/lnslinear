#include <stdint.h>

namespace flopoco {
    int64_t do_emulate(int64_t lxs[], int64_t lws[], int64_t b, int msb, int lsb,
                     int prevLayerWidth, int expLsb, bool isLastLayer);
}
